#ifndef CMenu
#define CMenu
#include "AbstractItemMenu.h"
//using namespace IEG;
namespace IEG
{
	class Cmenu
	{
		//protected:
		int m_select = -1;//�������� ���� ������������
		bool m_running = false;//�������� �� ���������� ����
		char* m_title = nullptr;//������ �� ������ � ���������� ����
		unsigned int m_count;//���-�� ������� ����
		ItemMenu* m_items = nullptr;//��������� �� ������ ��������
	public:
		Cmenu(const char*, ItemMenu*, unsigned int);
		int getSelect();
		bool getRunning();
		char* getTitle();
		unsigned int getCount();
		ItemMenu* getItems();
		void printItems();
		int runCommand();
		~Cmenu();
	};
}
#endif 

