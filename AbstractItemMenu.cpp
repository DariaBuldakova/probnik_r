#include "AbstractItemMenu.h"
#include <iostream>
const int Numb = 50;
using namespace IEG;

	ItemMenu::ItemMenu(const char* item_name, Func function)
	{
		this->m_item_name = new char[Numb];
		strcpy_s(this->m_item_name, Numb, item_name);
		m_func = function;
	}

	char* ItemMenu::get() //virtual
	{
		return this->m_item_name;
	}
	void ItemMenu::printItem() //virtual?
	{
		std::cout << this->m_item_name << std::endl;
	}
	int ItemMenu::run()
	{
		return this->m_func();
	}
	ItemMenu::~ItemMenu()
	{
		delete[]m_item_name;
	}



