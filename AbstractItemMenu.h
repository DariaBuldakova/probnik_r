#ifndef AbstractItemMenu
#define AbstractItemMenu

namespace IEG
{
	typedef int(*Func)();
	class ItemMenu
	{
	protected:
		char* m_item_name = nullptr;//��������� �� C-������ �������� ������
		Func m_func = nullptr;//��������� �� ����������� �������
	public:
		ItemMenu(const char*, Func);

		virtual char* get();
		
		virtual void printItem();
		
		virtual int run();

		~ItemMenu();
	};
}
#endif 
